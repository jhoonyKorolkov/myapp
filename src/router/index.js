import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import Begin from "@/views/Begin";
import Todo from "@/views/Todo";

const routes = [
  {
    path: "/",
    name: "begin",
    component: Begin,
  },
  {
    path: "/todo",
    name: "todo",
    component: Todo,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
